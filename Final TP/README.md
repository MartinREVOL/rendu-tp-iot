# Projet Final : Joystick

![](./Images/image.PF267946.en.feature-description-include-personalized-no-cpn-large.webp)

**Sommaire :**

- [Introduction](#introduction)
- [Étape 0 : Installation](#étape-0--installation)
- [Étape I : Setup](#étape-i--setup)
	- [🌞 Création du projet](#🌞-création-du-projet)
	- [🌞 Mise en place de STM32](#🌞-mise-en-place-de-stm32)
	- [🌞 Setup physique](#🌞-setup-physique)
- [Étape II : Le jeu](#étape-ii--le-jeu)
	- [🌞 Includes et déclaration de variables](#🌞-includes-et-déclaration-de-variables)
	- [🌞 Le code](#🌞-le-code)
- [Étape III : C'est parti !](#étape-iii--cest-parti)
	- [🌞 Putty](#🌞-putty)
	- [🌞 Lancement du jeu](#🌞-lancement-du-jeu)

## Introduction

Dans ce rendu de travail, nous allons vous présenter notre projet qui consistait à faire fonctionner un joystick sur la plateforme STM32CubeIDE.
Notre objectif était de pouvoir utiliser ce joystick pour contrôler les déplacements d'un jeu. 

Nous avons effectué des recherches approfondies pour comprendre le fonctionnement du joystick et comment le configurer pour qu'il soit compatible avec notre plateforme de développement. 
Nous avons ensuite mis en place une interface utilisateur conviviale pour permettre une utilisation facile et intuitive du joystick. 

Dans cette présentation, nous allons décrire les différentes étapes de notre projet, les défis que nous avons rencontrés et les solutions que nous avons mises en place pour y faire face. 

Enfin, nous allons discuter des résultats obtenus.

Bonne lecture !

## Étape 0 : Installation

Pour l'installation de STM32CubeIDE il vous faudra aller sur ce [lien](https://www.st.com/en/development-tools/stm32cubeide.html) et sélectionner la version corresppondante à votre OS dans `Get Software` en scrollant un peu sur la page :

![](./Images/Capture%20d%E2%80%99%C3%A9cran%202023-04-15%20%C3%A0%2011.20.29.png)

Pour la suite suivez les étapes d'installation de votre logiciel.

Dans ce projet nous allons aussi utiliser la console `putty`.
Voici un [lien Windows](https://www.ssh.com/academy/ssh/putty/windows/install), un [lien Linux](https://numato.com/blog/how-to-install-putty-on-linux/) et un [lien Macos](https://www.makeuseof.com/putty-for-mac/) qui vous expliqueront mieux que moi comment l'installer.

Pour finir cette étape, lancez CubeIDE et passez à la partie [Setup](#étape-i--setup).


## Étape I : Setup

### 🌞 Création du projet

Ça y est ! On rentre dans le vif du sujet ! Suivez-moi bien !

Après avoir lancer CubeIDE, cliquez sur `Start new STM32 project`, puis dans `Commercial number` tapez `STM32F401CBU7`, sélectionnez la version en bah à droite et cliquez sur `next`.

![](./Images/capder.png)

Ensuite on met le nom de notre projet, dans notre cas nous allons l'appeller `Adventure`.
Cliquez sur `Finish` et le projet vas se créer :

![](./Images/capture%201.png)

### 🌞 Mise en place de STM32

Il faut savoir que nous allons avoir besoin d'activer deux ports sur notre carte :
- un pour la position verticale du joystick (PB0)
- un pour la position horizontale du joystick (PA1)

Pour cela il faut activer les modes `IN1` et `IN8` de l'analogue `ADC1`, comme indiquer sur l'image ci-dessous :

![](./Images/1.png)

Puis nous allons ajouter un DMA settings en cliquant sur `Add` et en sélectionnant `Mode Circular` et `Data Width Bytes` :

![](./Images/2.png)

Faites quelques vérifications dans les settings en suivant les images en dessous :

`NVIC Settings`
![](./Images/3.png)

#### `GPIO Settings`
![](./Images/4.png)
On remarquera que le nom de nos PINs sont `PA1` pour `IN1` et `PB0` pour `IN8`.

`Parameter Settings`
![](./Images/5.png)
![](./Images/6.png)

Comme vu dans l'[installation](#étape-0--installation), nous allons utiliser putty, et pour ceal il nous faut activer `USART2` dans `Connectivity` et y mettre les bons paramètres :

![](./Images/7.png)

Et voilà ! Le setup de CubeIDE est fini !
Maintenant... On va brancher !

### 🌞 Setup physique

On sort notre belle carte graphique ci-dessous :
![](./Images/image1.jpg)

ET on branche les deux câbles en haut, un servira à l'alimentation, et l'autre pour la console putty.
![](./Images/image2.jpg)

On sort notre petit joystick et on branche le port `GND`au port `GND` et le port `+5V` au port `+3V`.
On le branche au `+3V` car notre materiel ne supporte tout simplement pas le `5V`.
![](./Images/image3.jpg)

Puis enfin, comme on a vu dans le [setup du projet](#gpio-settings), on branche `X` avec `PA1` et `Y` avec `PB0`.
![](./Images/image4.jpg)

ET voilàà le setup est terminé !!!
![](./Images/image5.jpg)

On va passer au code maintenant !!

## Étape II : Le jeu

Nous allons coder dans le fichier main.c.

### 🌞 Includes et déclaration de variables

On ajoutes des includes :
```c
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_host.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stm32f4xx.h"
#include "stdlib.h"
#include "stdio.h"
#include "time.h"
/* USER CODE END Includes */
```

Puis des variables qui nous seront indispensables pour le bon fonctionnement du code :
```c
/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

I2C_HandleTypeDef hi2c1;

I2S_HandleTypeDef hi2s3;

SPI_HandleTypeDef hspi1;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
uint8_t adcBuffer[2];
uint8_t v1, v2, x, y, Score, zero_x, zero_y;
/* USER CODE END PV */
```

Et enfin quelques fonctions :
```c
/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_I2C1_Init(void);
static void MX_I2S3_Init(void);
static void MX_SPI1_Init(void);
static void MX_ADC1_Init(void);
static void MX_USART2_UART_Init(void);
void MX_USB_HOST_Process(void);

/* USER CODE BEGIN PFP */
void ecris_char(uint8_t car);
/* USER CODE END PFP */
```

### 🌞 Le code

On entre dans la boucle main.
```c
int main(void)
{
```

On lance le timer et on setup Buffer pour pouvoir prendre le positionnement du joystick :
```c
/* USER CODE BEGIN 1 */
  time_t t;
  srand((unsigned)time(&t));
/* USER CODE END 1 */

/* USER CODE BEGIN 2 */
  HAL_ADC_Start_DMA(&hadc1, (uint32_t *)adcBuffer, 2);
/* USER CODE END 2 */
```

Et voici la suite du code expliqué à l'aide de commentaires clairs pour vous aider :
```c
/* Infinite loop */
  /* USER CODE BEGIN WHILE */

  //reset de l'affichage
  uint8_t clear[50]="\033[H\033[2J";
  ecris_txt(clear);

  //définir coordonnées de X
  x = 5;
  y = 10;

  //afficher X
  char pos[10];
  sprintf(pos, "\033[%d;%dH", x, y);
  uint8_t test[50];
  strcpy(test, pos);
  strcat(test, "\033[0;31mX\033[0m");
  ecris_txt(test);

  //générer des coordonnées pour 0
  zero_x = rand() % 15 +1;
  zero_y = rand() % 25 +1;

  //afficher 0
  char zero_pos[10];
  sprintf(zero_pos, "\033[%d;%dH", zero_x, zero_y);
  ecris_txt(zero_pos);
  ecris_txt("0");
  Score = 0;

  //début de la boucle infini
  while (1)
  {

      //définir buffers pour prendre les valeurs de position du joystick
	  v1 = adcBuffer[0];
	  v2 = adcBuffer[1];

      //afficher 0
	  char zero_pos[10];
	  sprintf(zero_pos, "\033[%d;%dH", zero_x, zero_y);
	  ecris_txt(zero_pos);
	  ecris_txt("0");

      //joystick à droite
	  if (v1 < 1)
	  {

          //clear le terminal
		  ecris_txt(clear);

          //déplacer le X vers la droite si X ne sort pas des valeurs max
		  if (y <25){
			  y +=1;
		  } else {
			  y =25;
		  }

          //afficher la nouvelle position de X
		  sprintf(pos, "\033[%d;%dH", x, y);
		  uint8_t test2[50];
		  strcpy(test2, pos);
		  strcat(test2, "\033[0;31mX\033[0m");
		  ecris_txt(test2);

          //allumer la led à droite (rouge)
		  HAL_GPIO_WritePin(LD5_GPIO_Port, LD5_Pin, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(LD4_GPIO_Port, LD4_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(LD6_GPIO_Port, LD6_Pin, GPIO_PIN_RESET);

            //si X est sur 0
			if (x == zero_x && y == zero_y)
			{
				//ajouter 1 au score
		    	Score = Score +1;

		        //effacer le 0
				ecris_txt(zero_pos);
				ecris_txt(" ");

		        //génération de nouvelles coordonnées pour "0"
				zero_x = rand() % 15 +1;
				zero_y = rand() % 25 +1;

		        //affichage de "0" aux nouvelles coordonnées
		        sprintf(zero_pos, "\033[%d;%dH", zero_x, zero_y);
		        ecris_txt(zero_pos);
		        ecris_txt("0");
				ecris_txt(test2);
			}
	  }

      //joystick à gauche
	  else if (v1 > 250)
	  {

          //clear le terminal
		  ecris_txt(clear);

          //déplacer le X vers la gauche si X ne sort pas des valeurs max
		  if (y >1){
			  y -=1;
		  } else {
			  y =1;
		  }

          //afficher la nouvelle position de X
		  sprintf(pos, "\033[%d;%dH", x, y);
		  uint8_t test3[50];
		  strcpy(test3, pos);
		  strcat(test3, "\033[0;31mX\033[0m");
		  ecris_txt(test3);

          //allumer la led à gauche (verte)
		  HAL_GPIO_WritePin(LD4_GPIO_Port, LD4_Pin, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(LD5_GPIO_Port, LD5_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(LD6_GPIO_Port, LD6_Pin, GPIO_PIN_RESET);

            //si X est sur 0
			if (x == zero_x && y == zero_y)
			{
				//ajouter 1 au score
		    	Score = Score +1;

		        //effacer le 0
				ecris_txt(zero_pos);
				ecris_txt(" ");

		        //génération de nouvelles coordonnées pour "0"
				zero_x = rand() % 15 +1;
				zero_y = rand() % 25 +1;

		        //affichage de "0" aux nouvelles coordonnées
		        sprintf(zero_pos, "\033[%d;%dH", zero_x, zero_y);
		        ecris_txt(zero_pos);
		        ecris_txt("0");
				ecris_txt(test3);
			}
	  }

      //joystick en bas
	  else if (v2 < 1)
	  {

          //clear le terminal
		  ecris_txt(clear);

          //déplacer le X vers le bas si X ne sort pas des valeurs max
		  if (x <15){
			  x +=1;
		  } else {
			  x =15;
		  }

          //afficher la nouvelle position de X
		  sprintf(pos, "\033[%d;%dH", x, y);
		  uint8_t test4[50];
		  strcpy(test4, pos);
		  strcat(test4, "\033[0;31mX\033[0m");
		  ecris_txt(test4);

          //allumer la led en bas (bleue)
		  HAL_GPIO_WritePin(LD6_GPIO_Port, LD6_Pin, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(LD5_GPIO_Port, LD5_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(LD4_GPIO_Port, LD4_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, GPIO_PIN_RESET);

            //si X est sur 0
		    if (x == zero_x && y == zero_y)
		    {

                //ajouter 1 au score
		    	Score = Score +1;

		        //effacer le 0
				ecris_txt(zero_pos);
				ecris_txt(" ");

		        //génération de nouvelles coordonnées pour "0"
				zero_x = rand() % 15 +1;
				zero_y = rand() % 25 +1;

		        //affichage de "0" aux nouvelles coordonnées
		        sprintf(zero_pos, "\033[%d;%dH", zero_x, zero_y);
		        ecris_txt(zero_pos);
		        ecris_txt("0");
		        ecris_txt(test4);
		    }
	  }

      //joystick en haut
	  else if (v2 > 250)
	  {

          //clear le terminal
		  ecris_txt(clear);

          //déplacer le X vers le haut si X ne sort pas des valeurs max
		  if (x >1){
			  x -=1;
		  } else {
			  x =1;
		  }

          //afficher la nouvelle position de X
		  sprintf(pos, "\033[%d;%dH", x, y);
		  uint8_t test5[50];
		  strcpy(test5, pos);
		  strcat(test5, "\033[0;31mX\033[0m");
		  ecris_txt(test5);

          //allumer la led en haut (jaune)
		  HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(LD5_GPIO_Port, LD5_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(LD4_GPIO_Port, LD4_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(LD6_GPIO_Port, LD6_Pin, GPIO_PIN_RESET);

            //si X est sur 0
		    if (x == zero_x && y == zero_y)
		    {

                //ajouter 1 au score
		    	Score = Score +1;

		        //effacer le 0
				ecris_txt(zero_pos);
				ecris_txt(" ");

		        //génération de nouvelles coordonnées pour "0"
		        zero_x = rand() % 15 +1;
		        zero_y = rand() % 25 +1;

		        //affichage de "0" aux nouvelles coordonnées
		        sprintf(zero_pos, "\033[%d;%dH", zero_x, zero_y);
		        ecris_txt(zero_pos);
		        ecris_txt("0");
		        ecris_txt(test5);
		    }
	  } else {

          //éteindre toutes les leds si le joystick n'a pas de position
		  HAL_GPIO_WritePin(LD5_GPIO_Port, LD5_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(LD4_GPIO_Port, LD4_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(LD6_GPIO_Port, LD6_Pin, GPIO_PIN_RESET);
	  }

      //ajouter un delay entre chaque mouvements (TRÈS IMPORTANT)
	  HAL_Delay(150);

      //afficher le score en bas à gauche
	  char pos[10];
	  sprintf(pos, "\033[16;1HScore: %d", Score);
	  uint8_t score[50];
	  strcpy(score, pos);
	  ecris_txt(score);



    /* USER CODE END WHILE */
    MX_USB_HOST_Process();

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}
```
Pour avoir le main.c complet, cliquez [ici](./main.c).

## Étape III : C'est parti !

### 🌞 Putty
Maintenant on setup putty yes !

Lancez-le et suivez les indication ci-dessous :

Voici sur quoi vous tombez en lançant putty :
![](./Images/8.png)

Voilà ce qu'il faut modifier :
![](./Images/9.png)
On voit ici que le `Serial Port` est branché sur `COM11`, donc on met `COM11` dans `Serial line`.
![](./Images/10.png)

### 🌞 Lancement du jeu

On compile le code en appuyant sur la flèche verte dans la barre d'outils :

![](./Images/ibhcez.png) 

Et la console putty va se lancer avec le jeu !

Utilisez le joystick pour capturer les `0` avec votre personnage ! Votre score s'affichera en bas à gauche de la console !! 

Amusez-vous bien !!

![](./Images/VID_20230406_122434_0_.gif)
