# Projet GPIO

![](../Final%20TP/Images/image.PF267946.en.feature-description-include-personalized-no-cpn-large.webp)

**Sommaire :**

- [Introduction](#introduction)
- [Étape 0 : Installation](#étape-0--installation)
- [Étape I : Setup](#étape-i--setup)
	- [🌞 Création du projet](#🌞-création-du-projet)
	- [🌞 Setup physique](#🌞-setup-physique)
- [Étape II : Le jeu](#étape-ii--le-jeu)
	- [🌞 Includes et déclaration de variables](#🌞-includes-et-déclaration-de-variables)
	- [🌞 Le code](#🌞-le-code)
- [Étape III : C'est parti !](#étape-iii--cest-parti)
	- [🌞 Lancement du jeu](#🌞-lancement-du-jeu)

## Introduction

Dans ce rendu de travail, nous allons vous présenter notre projet qui consistait à faire fonctionner les leds d'une carte graphique (STM32F407G-DISC1) sur la plateforme STM32CubeIDE.
Notre objectif était d'allumer les leds une par une dans un sens puis si le bouton User était appuyé, inverser le sens d'allumage des leds.

Nous avons effectué des recherches approfondies pour comprendre le fonctionnement des leds et comment les configurer.

Dans cette présentation, nous allons décrire les différentes étapes de notre projet, les défis que nous avons rencontrés et les solutions que nous avons mises en place pour y faire face. 

Enfin, nous allons discuter des résultats obtenus.

Bonne lecture !

## Étape 0 : Installation

Pour l'installation de STM32CubeIDE il vous faudra aller sur ce [lien](https://www.st.com/en/development-tools/stm32cubeide.html) et sélectionner la version corresppondante à votre OS dans `Get Software` en scrollant un peu sur la page :

![](../Final%20TP/Images/Capture%20d%E2%80%99%C3%A9cran%202023-04-15%20%C3%A0%2011.20.29.png)

Pour la suite suivez les étapes d'installation de votre logiciel.

Puis enfin, lancez CubeIDE et passez à la partie [Setup](#étape-i--setup).


## Étape I : Setup

### 🌞 Création du projet

Ça y est ! On rentre dans le vif du sujet ! Suivez-moi bien !

Après avoir lancer CubeIDE, cliquez sur `Start new STM32 project`, puis dans `Commercial number` tapez `STM32F401CBU7`, sélectionnez la version en bah à droite et cliquez sur `next`.

![](../Final%20TP/Images/capder.png)

Ensuite on met le nom de notre projet, dans notre cas nous allons l'appeller `leds`.
Cliquez sur `Finish` et le projet vas se créer :

![](../Final%20TP/Images/capture%201.png)

### 🌞 Setup physique

On sort notre belle carte graphique ci-dessous :
![](../Final%20TP/Images/image1.jpg)

ET on branche le câble en haut.
![](./Image/IMG_20230416_141403.jpg)

ET voilàà le setup est terminé !!!

## Étape II : Le jeu

Nous allons coder dans le fichier main.c.

### 🌞 Includes et déclaration de variables

On ajoutes des includes :
```c
#include "main.h"
#include "usb_host.h"
```

Puis des variables qui nous seront indispensables pour le bon fonctionnement du code :
```c
/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

I2S_HandleTypeDef hi2s3;

SPI_HandleTypeDef hspi1;
```

Et enfin quelques fonctions :
```c
/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_I2S3_Init(void);
static void MX_SPI1_Init(void);
void MX_USB_HOST_Process(void);
```

### 🌞 Le code

On entre dans la boucle main.
```c
int main(void)
{
```

On reset et initialise quelques variables :
```c
/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_I2C1_Init();
	MX_I2S3_Init();
	MX_SPI1_Init();
	MX_USB_HOST_Init();
```

Pour la suite voici la boucle `while(1)` expliqué à l'aide de commentaires clairs pour vous aider :
```c
while (1)
	{
		//Si le bouton est bien enfoncé
		if(HAL_GPIO_ReadPin(B1_GPIO_Port, B1_Pin) == GPIO_PIN_RESET) {
			
			//On fait clignoter les LEDs en séquence
			HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
			HAL_Delay(150);
			HAL_GPIO_TogglePin(LD5_GPIO_Port, LD5_Pin);
			HAL_Delay(150);
			HAL_GPIO_TogglePin(LD6_GPIO_Port, LD6_Pin);
			HAL_Delay(150);
			HAL_GPIO_TogglePin(LD4_GPIO_Port, LD4_Pin);
			HAL_Delay(150);
			
			// Si le bouton n'est pas enfoncé, fait clignoter les LEDs en sens inverse
		} else {
			HAL_GPIO_TogglePin(LD4_GPIO_Port, LD4_Pin);
			HAL_Delay(150);
			HAL_GPIO_TogglePin(LD6_GPIO_Port, LD6_Pin);
			HAL_Delay(150);
			HAL_GPIO_TogglePin(LD5_GPIO_Port, LD5_Pin);
			HAL_Delay(150);
			HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
			HAL_Delay(150);
		}
		/* USER CODE END WHILE */
		MX_USB_HOST_Process();

		/* USER CODE BEGIN 3 */
	}
```
Pour avoir le main.c complet, cliquez [ici](./main.c).

## Étape III : C'est parti !

### 🌞 Lancement du jeu

On compile le code en appuyant sur la flèche verte dans la barre d'outils :

![](../Final%20TP/Images/ibhcez.png)

Les LEDs vont clignoter en séquence dans le sens horaire !!
Appuyez sur le bouton User Bleu de votre carte et Tadaaa !
Les LEDs vont changer de sens de clignotement !!

Amusez-vous bien !!

![](./Image/T8ZQ.gif)
